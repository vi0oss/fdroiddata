Categories:Theming
License:MIT
Web Site:http://kisslauncher.com
Source Code:https://github.com/Neamar/KISS
Issue Tracker:https://github.com/Neamar/KISS/issues
Changelog:https://github.com/Neamar/KISS/releases

Auto Name:KISS launcher
Summary:Custom launcher
Description:
KISS is a fast launcher following the KISS principle.

Search through you app, contacts and settings lightning fast. No more time spent
trying to find the app you need to launch: enter a few characters from the name
and press enter. Need to phone someone? Don't meddle with the call log, just
give three letters of their name and push the "phone" button.

KISS becomes smarter and smarter as you use it, pushing forward results you're
more likely to select.
.

Repo Type:git
Repo:https://github.com/Neamar/KISS

Build:2.6.0,44
    commit=v2.6.0
    subdir=app
    gradle=yes

Build:2.6.4,48
    commit=v2.6.4
    subdir=app
    gradle=yes

Build:2.6.5,49
    commit=v2.6.5
    subdir=app
    gradle=yes

Build:2.7.2,52
    commit=v2.7.2
    subdir=app
    gradle=yes

Build:2.7.3,53
    commit=v2.7.3
    subdir=app
    gradle=yes

Build:2.7.4,54
    commit=v2.7.4
    subdir=app
    gradle=yes

Build:2.8.0,57
    commit=v2.8.0
    subdir=app
    gradle=yes

Build:2.9.3,61
    commit=v2.9.3
    subdir=app
    gradle=yes

Build:2.9.4,62
    commit=v2.9.4
    subdir=app
    gradle=yes

Build:2.10.1,64
    commit=v2.10.1
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
# Beta releases will be tagged as v%v-beta and should not be build by F-Droid
Update Check Mode:Tags ^v[0-9\.]+$
Current Version:2.10.1
Current Version Code:64
