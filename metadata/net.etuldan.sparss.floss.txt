Categories:Reading,Internet
License:GPLv3
Web Site:https://github.com/Etuldan/spaRSS/blob/HEAD/README.md
Source Code:https://github.com/Etuldan/spaRSS
Issue Tracker:https://github.com/Etuldan/spaRSS/issues

Auto Name:spaRSS
Summary:Feed reader
Description:
Based on [[net.fred.feedex]] and [[de.shandschuh.sparserss]], this checks
RSS/Atom news feeds, polling for updates from the device on a regular basis.
Fetched items are available for offline reading.
.

Repo Type:git
Repo:https://github.com/Etuldan/spaRSS

Build:1.9.2,58
    disable=remove apk
    commit=75209275768377bf5885a64ce0fd614038911afc
    subdir=mobile
    gradle=floss
    srclibs=TextDrawable@558677ea316e60346948b381e5e274f49b00d370
    rm=mobile/src/main/java/net/fred/feedex/wear/,wear/
    prebuild=sed -i -e '/maven {/,+2d' -e '/gms/d' -e '/textdrawable/d' build.gradle && \
        mkdir -p ../libs && \
        cp -fR $$TextDrawable$$ ../libs/textdrawable && \
        echo -e '\n\ninclude ":libs:textdrawable"' >> ../settings.gradle && \
        sed -i -e '/fileTree/acompile project(":libs:textdrawable")' build.gradle && \
        sed -i -e '/appcompat-v7/d' build.gradle && \
        sed -i -e '/fileTree/acompile "com.android.support:appcompat-v7:21.0.0"' build.gradle

Build:1.9.3,59
    commit=92e83112440141bb0c892002901f8fde4274a60b
    subdir=mobile
    gradle=floss
    srclibs=TextDrawable@558677ea316e60346948b381e5e274f49b00d370
    rm=mobile/src/main/java/net/fred/feedex/wear/,wear/
    prebuild=sed -i -e '/maven {/,+2d' -e '/gms/d' -e '/textdrawable/d' build.gradle && \
        mkdir -p ../libs && \
        cp -fR $$TextDrawable$$ ../libs/textdrawable && \
        echo -e '\n\ninclude ":libs:textdrawable"' >> ../settings.gradle && \
        sed -i -e '/fileTree/acompile project(":libs:textdrawable")' build.gradle

Build:1.10.0,60
    commit=v1.10
    subdir=mobile
    gradle=floss
    srclibs=TextDrawable@558677ea316e60346948b381e5e274f49b00d370
    forceversion=yes
    rm=mobile/src/main/java/net/fred/feedex/wear/,wear/
    prebuild=sed -i -e '/maven {/,+2d' -e '/gms/d' -e '/textdrawable/d' build.gradle && \
        mkdir -p ../libs && \
        cp -fR $$TextDrawable$$ ../libs/textdrawable && \
        echo -e '\n\ninclude ":libs:textdrawable"' >> ../settings.gradle && \
        sed -i -e '/fileTree/acompile project(":libs:textdrawable")' build.gradle

Build:1.10.1,62
    commit=1.10.1
    subdir=mobile
    gradle=floss
    srclibs=TextDrawable@558677ea316e60346948b381e5e274f49b00d370
    forceversion=yes
    rm=mobile/src/main/java/net/fred/feedex/wear/,wear/
    prebuild=sed -i -e '/maven {/,+2d' -e '/gms/d' -e '/textdrawable/d' build.gradle && \
        mkdir -p ../libs && \
        cp -fR $$TextDrawable$$ ../libs/textdrawable && \
        echo -e '\n\ninclude ":libs:textdrawable"' >> ../settings.gradle && \
        sed -i -e '/fileTree/acompile project(":libs:textdrawable")' build.gradle

Build:1.10.2,63
    commit=1.10.2
    subdir=mobile
    gradle=floss
    srclibs=TextDrawable@558677ea316e60346948b381e5e274f49b00d370
    forceversion=yes
    rm=mobile/src/main/java/net/fred/feedex/wear/,wear/
    prebuild=sed -i -e '/maven {/,+2d' -e '/gms/d' -e '/textdrawable/d' build.gradle && \
        mkdir -p ../libs && \
        cp -fR $$TextDrawable$$ ../libs/textdrawable && \
        echo -e '\n\ninclude ":libs:textdrawable"' >> ../settings.gradle && \
        sed -i -e '/fileTree/acompile project(":libs:textdrawable")' build.gradle

Build:1.10.3,64
    commit=1.10.3
    subdir=mobile
    gradle=floss
    srclibs=TextDrawable@558677ea316e60346948b381e5e274f49b00d370
    forceversion=yes
    rm=mobile/src/main/java/net/fred/feedex/wear/,wear/
    prebuild=sed -i -e '/maven {/,+2d' -e '/gms/d' -e '/textdrawable/d' build.gradle && \
        mkdir -p ../libs && \
        cp -fR $$TextDrawable$$ ../libs/textdrawable && \
        echo -e '\n\ninclude ":libs:textdrawable"' >> ../settings.gradle && \
        sed -i -e '/fileTree/acompile project(":libs:textdrawable")' build.gradle

Build:1.10.4,65
    commit=1.10.4
    subdir=mobile
    gradle=floss
    srclibs=TextDrawable@558677ea316e60346948b381e5e274f49b00d370
    forceversion=yes
    rm=mobile/src/main/java/net/fred/feedex/wear/,wear/
    prebuild=sed -i -e '/maven {/,+2d' -e '/gms/d' -e '/textdrawable/d' build.gradle && \
        mkdir -p ../libs && \
        cp -fR $$TextDrawable$$ ../libs/textdrawable && \
        echo -e '\n\ninclude ":libs:textdrawable"' >> ../settings.gradle && \
        sed -i -e '/fileTree/acompile project(":libs:textdrawable")' build.gradle

Auto Update Mode:None
Update Check Mode:Tags
Update Check Name:net.etuldan.sparss
Current Version:1.10.4
Current Version Code:65
