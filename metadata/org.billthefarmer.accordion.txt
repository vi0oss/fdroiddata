Categories:Multimedia
License:GPLv3
Web Site:https://github.com/billthefarmer/accordion/wiki
Source Code:https://github.com/billthefarmer/accordion
Issue Tracker:https://github.com/billthefarmer/accordion/issues

Auto Name:Accordion
Summary:Accordion emulator for tablets
Description:
Emulates a three row diatonic or continental chromatic accordion. There is a
choice of midi instruments and keys, including C System and B System.

* Keys: F/Bb/Eb, G/C/F, A/D/G, C#/D/G, B/C/C#, C System, B System
* Instruments: Standard midi set, defaults to accordion
* Choice of one standard 31 button Hohner layout and two 25 button layouts
* Choice of fascia images

Uses undocumented built in Sonivox midi synthesizer for midi audio output.
.

Repo Type:git
Repo:https://github.com/billthefarmer/accordion

Build:1.01,101
    commit=v1.01
    extlibs=arch-arm/libsonivox.so
    scanignore=libs/libsonivox.so
    build=mkdir -p ndk-r8e/toolchains && \
        find $$NDK$$ -maxdepth 1 -mindepth 1 -not -name toolchains -print0 | xargs -0 cp -r -t ndk-r8e/ && \
        cp -r $$NDK$$/toolchains/arm-linux-androideabi-4.6/ ndk-r8e/toolchains/ && \
        mv libs/libsonivox.so ndk-r8e/platforms/android-14/arch-arm/usr/lib/ && \
        ./ndk-r8e/ndk-build && \
        rm -rf ndk-r8e/
    buildjni=no

Build:1.02,102
    commit=v1.02
    extlibs=arch-arm/libsonivox.so
    scanignore=libs/libsonivox.so
    scandelete=libs/armeabi/libmidi.so
    build=mkdir -p ndk-r9d/toolchains && \
        find $$NDK$$ -maxdepth 1 -mindepth 1 -not -name toolchains -print0 | xargs -0 cp -r -t ndk-r9d/ && \
        cp -r $$NDK$$/toolchains/arm-linux-androideabi-4.6/ ndk-r9d/toolchains/ && \
        mv libs/libsonivox.so ndk-r9d/platforms/android-17/arch-arm/usr/lib/ && \
        ./ndk-r9d/ndk-build && \
        rm -rf ndk-r9d/
    buildjni=no

Build:1.03,103
    commit=v1.03
    scanignore=jni/libs
    scandelete=libs
    buildjni=yes

Build:1.04,104
    commit=v1.04
    scanignore=jni/libs
    scandelete=libs
    buildjni=yes

Build:1.05,105
    commit=v1.05
    scanignore=jni/libs
    scandelete=libs
    buildjni=yes

Build:1.06,106
    commit=v1.06
    scandelete=libs
    buildjni=yes

Maintainer Notes:
As of version 1.03, does not require the convoluted build process used
for previous versions. This is because the build process:

* Builds for arm, mips and x86 architectures
* Uses prebuilt shared libraries
* Uses custom rules to remove prebuilt shared libraries after NDK build and before SDK build
.

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.06
Current Version Code:106
