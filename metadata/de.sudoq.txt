Categories:Games
License:GPLv3+
Web Site:
Source Code:https://github.com/SDEagle/SudoQ
Issue Tracker:https://github.com/SDEagle/SudoQ/issues

Auto Name:SudoQ
Summary:Play sudokus
Description:
Play many sudoku types such as 16x16, squiggly or Samurai. There are many
assistances available, like back and forth, solve specific field ore back to
last correct status. Also, every state of the game is recorded in the history.
Also supported is gesture input (draw number on the screen).
.

Repo Type:git
Repo:https://github.com/SDEagle/SudoQ

Build:1.0.5,7
    commit=v1.0.5
    subdir=sudoq-app
    srclibs=1:ActionBarSherlock@4.4.0
    prebuild=cp -r ../sudoq-model/src/de src/ && \
        mkdir assets && \
        cp -r ../sudokus assets/

Build:1.0.6,10
    commit=v1.0.6
    subdir=sudoq-app
    srclibs=1:ActionBarSherlock@4.4.0
    prebuild=cp -r ../sudoq-model/src/de src/ && \
        mkdir assets && \
        cp -r ../sudokus assets/

Build:1.0.7,11
    commit=v1.0.7
    subdir=sudoq-app
    srclibs=1:ActionBarSherlock@4.4.0
    prebuild=mkdir assets && \
        cp -r ../sudokus assets/ && \
        sed -i -e 's;source="1.6" target="1.6";source="1.7" target="1.7";' ../sudoq-model/build.xml && \
        sed -i -e 's;../app/libs/;../sudoq-app/libs/;' ../sudoq-model/build.xml

Maintainer Notes:
The sed lines are needed for a bug of the build.xml in upstream.
Check if https://github.com/SDEagle/SudoQ/issues/120 is fixed at
the next release.
.

Auto Update Mode:None
# Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.0.7
Current Version Code:11
