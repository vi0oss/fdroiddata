Categories:Science & Education
License:GPLv2
Web Site:
Source Code:https://github.com/EvanRe/Equate
Issue Tracker:https://github.com/EvanRe/Equate/issues

Auto Name:Equate
Summary:Convert units
Description:
Unit converting calculator featuring:

* clean and fast unit conversions
* more than 10 different unit types, including real time currency
* over 130 units to convert between
* customizable unit buttons
* semi-scientific calculator
* order of operations
* operation history, with recall
* smart parenthesis
.

Repo Type:git
Repo:https://github.com/EvanRe/Equate

Build:1.0,1
    commit=3336b86508ac235e07c54ef69e46d1220e8fad26
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1
